#ifndef HILPARAMETER_H
#define HILPARAMETER_H

#include <string>
#include <cstdint>

#include "hilstreamunit.h"

class HILParameter
{
public:
    enum DataType{
        U64,
        U32,
        U16,
        S64,
        S32,
        S16,
        BOOL,
        FLOAT
    };

    //Different constructors for different data sizes.
    HILParameter(const std::string& name, const uint64_t& memoryaddress, const uint64_t& mask, DataType type, const int64_t& value):
        Name(name), MemoryAddress(memoryaddress), Mask(mask), Type(type), Value(value){if(Mask) setValue(value);}
    HILParameter(const std::string& name, const uint64_t& memoryaddress, const uint64_t& mask, DataType type, const int32_t& value):
        Name(name), MemoryAddress(memoryaddress), Mask(mask), Type(type), Value(value){if(Mask) setValue(value);}
    HILParameter(const std::string& name, const uint64_t& memoryaddress, const uint64_t& mask, DataType type, const int16_t& value):
        Name(name), MemoryAddress(memoryaddress), Mask(mask), Type(type), Value(value){if(Mask) setValue(value);}
    HILParameter(const std::string& name, const uint64_t& memoryaddress, const uint64_t& mask, DataType type, const bool& value):
        Name(name), MemoryAddress(memoryaddress), Mask(mask), Type(type), Value(value){if(Mask) setValue(value);}

    //Data manipulation
    void setValue(const int64_t& value){
        if(Type == DataType::U64 || Type == DataType::S64 || Type == DataType::FLOAT)
            Value.setValue(static_cast<int64_t>( Mask ? applyMask(value) : value ));
        else if(Type == DataType::U32 || Type == DataType::S32)
            Value.setValue(static_cast<int32_t>( Mask ? applyMask(value) : value ));
        else if(Type == DataType::U16 || Type == DataType::S16)
            Value.setValue(static_cast<int16_t>( Mask ? applyMask(value) : value ));
        else if(Type == DataType::BOOL)
            Value.setValue( (Mask ? applyMask(value) : value) ? true : false);

    }

    //Access data
    std::string getName() const {return Name;}
    uint64_t getMask() const {return Mask;}
    uint64_t getMemoryAddress() const {return MemoryAddress;}
    DataType getType() const {return Type;}
    int64_t getValue() const {
        if(Type == DataType::U64 || Type == DataType::S64 || Type == DataType::FLOAT)
            return Value.get64Value();
        else if(Type == DataType::U32 || Type == DataType::S32)
            return static_cast<int64_t>(Value.get32Value());
        else if(Type == DataType::U16 || Type == DataType::S16)
            return static_cast<int64_t>(Value.get16Value());
        else if(Type == DataType::BOOL)
            return Value.get64Value();

        return 0;
    }

    //For interface
    int64_t inverseMask();
    int64_t inverseMask(const int64_t &value);

private:
    std::string Name;
    uint64_t MemoryAddress;
    uint64_t Mask;
    DataType Type;
    HILStreamUnit::UniValue Value;

    int64_t applyMask(const int64_t &value);
};

#endif // HILPARAMETER_H
