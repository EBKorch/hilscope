#ifndef HILCONTROL_H
#define HILCONTROL_H

#include <QObject>
#include <QVariant>
#include <QList>

#include "hilparameterhandler.h"
#include "fileinterface.h"

class HILControl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString paramfile READ paramfile WRITE setParamfile NOTIFY paramfileChanged)
public:
    explicit HILControl(QObject *parent = nullptr);

    QString name();
    void setName(const QString& name);

    QString paramfile();
    void setParamfile(const QString& paramfile);

    enum Interface{
        FileIO,
        FileIORaw
    };
    Q_ENUM(Interface)

    Q_INVOKABLE bool createHandler(Interface iointerface = Interface::FileIO);
    Q_INVOKABLE unsigned long getSignalNumber();
    Q_INVOKABLE QString getParameterName(const unsigned long& internalid);
    Q_INVOKABLE QString getParameterValue(const unsigned long& internalid, const int &type = 0);
    Q_INVOKABLE QString getParameterType(const unsigned long &internalid);
    Q_INVOKABLE void getSpecificUpdates(const QVariantList& internalids);
    Q_INVOKABLE void setSpecificValues(const QVariantList& internalis, const QVariantList& values);
    Q_INVOKABLE void logVariable(const unsigned long &internalid);
    Q_INVOKABLE void clearVariableLogs(const QList<QVariant> &internalids);

signals:
    void nameChanged();
    void paramfileChanged();

private:
    HILParameterHandler *handler;

    QString m_name;
    QString m_paramfile;
    HILAbstractInterface *interface;

    struct Record{
        std::chrono::time_point<std::chrono::system_clock> timestamp;
        double value;
    };
    struct LogsForID{
        unsigned long internalID;
        std::vector<Record> records;
    };

    std::vector<LogsForID> ValueLogs;
};

#endif // HILCONTROL_H
