#include "hilcontrol.h"

HILControl::HILControl(QObject *parent) : QObject(parent), handler(nullptr)
{

}

QString HILControl::name(){return m_name;}

void HILControl::setName(const QString &name){
    if(name == m_name)
        return;

    m_name = name;
    emit nameChanged();
}

QString HILControl::paramfile(){return m_paramfile;}

void HILControl::setParamfile(const QString &paramfile){
    if(paramfile == m_paramfile)
        return;

    m_paramfile = paramfile;
    emit paramfileChanged();
}

bool HILControl::createHandler(Interface iointerface){
    //remove if there is already a handler declared, return value will indicate is this action was performed
    bool redefine = false;
    if(handler != nullptr){
        delete handler;
        redefine = true;
    }


    std::cout << iointerface << std::endl;
    //create the requested interface, no else branch is needed, as iointerface argument has a default value
    if(iointerface == Interface::FileIO)
        interface = new FileInterface("Test.txt");
    else if(iointerface == Interface::FileIORaw)
        interface = new FileInterface("RAM");

    //create the handler itself, passing the proper interface
    handler = new HILParameterHandler(m_name.toStdString(), m_paramfile.toStdString(), *interface);

    return redefine;
}

unsigned long HILControl::getSignalNumber(){
    if(handler)
        return handler->getParameterCount();
    else{
        qWarning("Handler is not set!");
        return 0;
    }
}

QString HILControl::getParameterName(const unsigned long& internalid){
    if(handler)
        return QString::fromStdString(handler->getName(internalid));
    else{
        qWarning("Handler is not set!");
        return "";
    }
}

QString HILControl::getParameterValue(const unsigned long& internalid, const int& type){
    if(!handler){
        qWarning("Handler is not set!");
        return "";
    }

    //get raw (uint64_t) value from handler
    int64_t value = handler->getValue(internalid);
    std::string stringValue;

    //Qt/QML expects a string return value, therefore conversion is needed
    if(type == 0){
        //if user not specified a type, use conversion based on parameter type got from hardware
        HILParameter::DataType t = handler->getType(internalid);

        if(t == HILParameter::DataType::U64){
            stringValue = std::to_string(value);
        }
        else if(t == HILParameter::DataType::U32){
            stringValue = std::to_string(static_cast<uint32_t>(value));
        }
        else if(t == HILParameter::DataType::U16){
            stringValue = std::to_string(static_cast<uint16_t>(value));
        }
        else if(t == HILParameter::DataType::S16){
            stringValue = std::to_string(static_cast<int16_t>(value));
        }
        else if(t == HILParameter::DataType::FLOAT){
            //special conversion needed in this case, use memcpy - only method which has officially documented behaviour
            double dvalue;
            std::memcpy(&dvalue,&value,sizeof(double));
            stringValue = std::to_string(dvalue);
        }
        else stringValue = std::to_string(value); //fallback mode - no special conversion
    }
    else{
        double dvalue;
        switch(type){
        case 1: //INT64
            stringValue = std::to_string(static_cast<int64_t>(value)); break;
        case 2: //INT32
            stringValue = std::to_string(static_cast<int32_t>(value)); break;
        case 3: //INT16
            stringValue = std::to_string(static_cast<int16_t>(value)); break;
        case 4: //INT8
            stringValue = std::to_string(static_cast<int8_t>(value)); break;
        case 5: //UINT64
            stringValue = std::to_string(static_cast<uint64_t>(value)); break;
        case 6: //UINT32
            stringValue = std::to_string(static_cast<uint32_t>(value)); break;
        case 7: //UINT16
            stringValue = std::to_string(static_cast<uint16_t>(value)); break;
        case 8: //UINT8
            stringValue = std::to_string(static_cast<uint8_t>(value)); break;
        case 9: //LOGIC
            stringValue = value == 0 ? '0' : '1'; break;
        case 10: //DOUBLE
            std::memcpy(&dvalue,&value,sizeof(double));
            stringValue = std::to_string(dvalue); break;
        }
    }

    //return with the created string - one extra conversion needed for Qt compatibility
    return QString::fromStdString(stringValue);
}

QString HILControl::getParameterType(const unsigned long &internalid){
    if(!handler){
        qWarning("Handler is not set!");
        return "";
    }

    //retrieve data type from handler and return with a string which can be processed in QML
    HILParameter::DataType type =  handler->getType(internalid);

    if(type == HILParameter::DataType::U64)
        return QString("Unsigned 64bit");
    else if(type == HILParameter::DataType::U32)
        return QString("Unsigned 32bit");
    else if(type == HILParameter::DataType::U16)
        return QString("Unsigned 16bit");
    else if(type == HILParameter::DataType::S64)
        return QString("Signed 64bit");
    else if(type == HILParameter::DataType::S32)
        return QString("Signed 32bit");
    else if(type == HILParameter::DataType::S16)
        return QString("Signed 16bit");
    else if(type == HILParameter::DataType::BOOL)
        return QString("Logic");
    else if(type == HILParameter::DataType::FLOAT)
        return QString("Float");
    else
        return QString("Unknown - defaulting to U64");

}

void HILControl::getSpecificUpdates(const QList<QVariant>& internalids){
    if(!handler){
        qWarning("Handler is not set!");
    }
    else{
        //convert the QVariant internalIDs to standard unsigned long
        std::vector<unsigned long> internalidsVect;
        for(int i = 0; i < internalids.size(); i++)
            internalidsVect.push_back(static_cast<unsigned long>(internalids.value(i).toInt()));

        //request specific updates from handler to parameters
        handler->updateValues(internalidsVect);
    }
}

void HILControl::setSpecificValues(const QList<QVariant>& internalids, const QList<QVariant>& values){
    if(!handler){
        qWarning("Handler is not set!");
    }
    else{
        //push every change to the parameters
        for(int i = 0; i < internalids.size(); i++){
            handler->pushChangeToValue(static_cast<unsigned long>(internalids.value(i).toInt()),
                                       static_cast<double>(values.value(i).toDouble()));
        }

        //push pending changes to 'hardware' (push them to the interface)
        handler->pushPendingChanges();
    }
}

void HILControl::logVariable(const unsigned long &internalid){
    //create the new record
    Record rec = {std::chrono::system_clock::now(),static_cast<double>(handler->getValue(internalid))};

    //find if there is existing logs for this id
    auto iterator = std::find_if(ValueLogs.begin(),ValueLogs.end(),[&internalid](const LogsForID &tmp)
                                {return tmp.internalID == internalid;});

    if(iterator == ValueLogs.end()){
        //there is no record for that id yet, create the initial record
        std::vector<Record> records; records.push_back(rec);
        LogsForID newLogID = {internalid,records};
        ValueLogs.push_back(newLogID);
    }
    else{
        //there are recors already for that id, just push the new one
        iterator->records.push_back(rec);
    }
}

void HILControl::clearVariableLogs(const QList<QVariant> &internalids){
    for(int i = 0; i < internalids.size(); i++){
        //obtain the current ID by converting the QVariant element in the list to int
        unsigned long currID = static_cast<unsigned long>(internalids.value(i).toInt());

        //search for the current ID among the logs
        auto iterator = std::find_if(ValueLogs.begin(),ValueLogs.end(),[&currID](const LogsForID &tmp){return tmp.internalID == currID;});

        //if ID is found delete the entry from it
        if(iterator != ValueLogs.end())
            ValueLogs.erase(iterator);
    }
}
