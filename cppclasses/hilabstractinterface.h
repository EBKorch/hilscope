#ifndef HILABSTRACTINTERFACE_H
#define HILABSTRACTINTERFACE_H

#include <vector>
#include <cstdint>

#include "hilstreamunit.h"

class HILAbstractInterface
{
public:
    HILAbstractInterface(){}

    enum StatusInfo{
        INWRITE,
        INREAD,
        IDLING
    };

    virtual std::vector<HILStreamUnit> recieveInput() = 0;
    virtual std::vector<HILStreamUnit> recieveSpecificInput(std::vector<uint64_t> memoryaddresses) = 0;
    virtual void sendOutput(const std::vector<HILStreamUnit>& stream) = 0;

    StatusInfo getStatus(){return status;}

    virtual ~HILAbstractInterface(){}

protected:
    StatusInfo status;
};

#endif // HILABSTRACTINTERFACE_H
