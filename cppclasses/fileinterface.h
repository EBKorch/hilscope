#ifndef FILEINTERFACE_H
#define FILEINTERFACE_H

#include "hilabstractinterface.h"
#include "hilstreamunit.h"
#include <string>
#include <thread>
#include <iostream>
#include <fstream>
#include <algorithm>

class FileInterface : public HILAbstractInterface{
    std::string filename;
public:
    FileInterface(const std::string& filename): filename(filename){status = StatusInfo::IDLING;}

    void sendOutput(const std::vector<HILStreamUnit>& stream) {
        int trys = 10;
        while(status != StatusInfo::IDLING){std::this_thread::sleep_for(std::chrono::milliseconds(100)); --trys;};

        if(status == StatusInfo::IDLING){
            status = StatusInfo::INWRITE;

            std::rename(filename.c_str(),"saf");

            std::ifstream saffile("saf");
            std::ofstream outfile(filename);

            trys = 10;
            while(! outfile.good() && trys){
                outfile.open(filename);
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                --trys;
            }

            if(outfile.is_open() && saffile.is_open()){
                std::string currLine = "";

                std::vector<bool> isWritten;
                for(unsigned long i = 0; i < stream.size(); i++)
                    isWritten.push_back(false);

                while(getline(saffile,currLine)){
                    std::string memaddressStr = currLine.substr(0,currLine.find(' '));
                    uint64_t addressOfLine = std::stoul(memaddressStr,nullptr,16);
                    auto iterator = std::find_if(stream.begin(),stream.end(),[&addressOfLine](const HILStreamUnit &tmp){
                                                                            return tmp.getMemoryAddress() == addressOfLine;});

                    if(iterator == stream.end())
                        outfile << currLine << std::endl;
                    else{
                        outfile << "0x" << std::hex << addressOfLine << std::dec << " " << iterator->getValue() << std::endl;
                        isWritten[std::distance(stream.begin(),iterator)] = true;
                    }
                }

                for(unsigned long i = 0; i < isWritten.size(); i++){
                    if(!isWritten[i]){
                        outfile << "0x" << std::hex << stream[i].getMemoryAddress() << std::dec << " " << stream[i].getValue() << std::endl;
                    }
                }

                saffile.close();
                remove("saf");

                outfile.close();
            }
            else std::cout << "Cannot open files for write" << std::endl;

            status = StatusInfo::IDLING;
        }
    }
    virtual std::vector<HILStreamUnit> recieveInput() {
        int trys = 10;
        while(status != StatusInfo::IDLING && trys){std::this_thread::sleep_for(std::chrono::milliseconds(100)); --trys;};

        std::vector<HILStreamUnit> ret;

        if(status == StatusInfo::IDLING){
            status = StatusInfo::INREAD;

            std::ifstream infile(filename);

            trys = 10;
            while((! infile.good()) && trys){
                infile.open(filename);
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                --trys;
            }

            std::string line = "";
            if(infile.is_open()){
                while(getline(infile,line)){
                    uint64_t memaddress = static_cast<uint64_t>(std::stoull(line.substr(0,line.find(' ')),nullptr,16));
                    line = line.substr(line.find(' ')+1);
                    int64_t value = static_cast<int64_t>(std::stoll(line));
                    HILStreamUnit temp(memaddress,value);
                    ret.push_back(temp);
                }

                infile.close();
            }

            status = StatusInfo::IDLING;
        }

        return ret;
    }
    virtual std::vector<HILStreamUnit> recieveSpecificInput(std::vector<uint64_t> memoryaddresses) {
        int trys = 10;
        while(status != StatusInfo::IDLING && trys){std::this_thread::sleep_for(std::chrono::milliseconds(100)); --trys;};

        std::vector<HILStreamUnit> ret;

        if(status == StatusInfo::IDLING){
            status = StatusInfo::INREAD;

            std::ifstream infile(filename);

            std::string line = "";

            trys = 10;
            while((! infile.good()) && trys){
                infile.open(filename);
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                --trys;
            }

            if(infile.is_open()){
                while(getline(infile,line)){
                    uint64_t memaddress = static_cast<uint64_t>(std::stoull(line.substr(0,line.find(' ')),nullptr,16));

                    auto explorer = std::find(memoryaddresses.begin(),memoryaddresses.end(),memaddress);
                    if(explorer != memoryaddresses.end()){
                        line = line.substr(line.find(' ')+1);
                        int64_t value = static_cast<int64_t>(std::stod(line));
                        HILStreamUnit temp(memaddress,value);
                        ret.push_back(temp);
                    }
                }
            }

            status = StatusInfo::IDLING;
        }

        return ret;
    }

    ~FileInterface(){}
};

#endif // FILEINTERFACE_H
