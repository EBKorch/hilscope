import QtQuick 2.11
import QtQuick.Controls.Material 2.3


Item {
    //contains the X-Y value pairs which should be plotted
    property var valuesX
    property var valuesY

    //plot scaling related
    property bool fitIntoFrame: true
    property bool customScalingFactor: false
    property double xScalingFactor: 1
    property double yScalingFactor: 1

    //properties for moving the plot
    property double xOffset: dummyoffset.x+dummydrag.x
    property double yOffset: dummyoffset.y+dummydrag.y

    //visuals of axes
    property int axesWidth:   1
    property int tickHeight:  3
    property int tickWidth:   1
    property int xSkip:       0
    property int ySkip:       0
    property int xAxisOffset: 0
    property int yAxisOffset: 0
    property int minTickDistance: 15
    //0 - no numbers on axes, 1 - only on x, 2 - only on y, 3 or higher - on both axes
    property int showAxesNumbers: 3

    //style of lines - can be array for multiple input series
    property int lineWidth:  2
    property var lineColors
    property var zeroOrderHold


    //wrapper to make it easy to update the canvas
    function requestPaint(){displayplot.requestPaint()}

    //sets the scaling factor to fit the every value into the canvas
    function fitIntoCanvas(){

        //FIND LIMITS FOR BOTH VALUE SET
        function boundsOfSeries(xSeries,ySeries){
            var minLength = xSeries.length > ySeries.length ? ySeries.length : xSeries.length
            var minX = xSeries[0], maxX = xSeries[0]
            var minY = ySeries[0], maxY = ySeries[0]
            for(var i = 1; i < minLength; i++){
                //for valuesX
                if(xSeries[i] < minX) minX = xSeries[i]
                else if(xSeries[i] > maxX) maxX = ySeries[i]

                //for valuesY
                if(ySeries[i] < minY) minY = ySeries[i]
                else if(ySeries[i] > maxY) maxY = ySeries[i]
            }

            return [minX,maxX,minY,maxY]
        }

        //only iterate if there is value in both array - only those with pair will be plotted

        var currXSeries = valuesX[0]
        var currYSeries = valuesY[0]
        var boundaries
        if(currXSeries.length && currYSeries.length){
            var minLength = valuesX.length > valuesY.length ? valuesY.length : valuesX.length
            boundaries = boundsOfSeries(valuesX[0],valuesY[0])
            for(var i = 1; i < minLength; i++){
                var tmp = boundsOfSeries(valuesX[i],valuesY[i])
                if(tmp[0]<boundaries[0]) boundaries[0] = tmp[0]
                if(tmp[1]>boundaries[1]) boundaries[1] = tmp[1]
                if(tmp[2]<boundaries[2]) boundaries[2] = tmp[2]
                if(tmp[3]>boundaries[3]) boundaries[3] = tmp[3]
            }
        }
        else boundaries = boundsOfSeries(valuesX,valuesY)

        //CALCULATE SCALING FACTOR

        //only use maximum values to ensure that the origo is on the canvas
        xScalingFactor = width/boundaries[1]
        yScalingFactor = height/boundaries[3]

        updateTickSkip()
    }

    function updateTickSkip(){
        //do not let ticks touch each other
        var skipTick = 0
        while( (yScalingFactor*Math.pow(2,skipTick) > minTickDistance || xScalingFactor*Math.pow(2,skipTick) > minTickDistance ) && skipTick > 0 ) //if it needs to be smaller
            skipTick -= 1
        while(xScalingFactor*Math.pow(2,skipTick) < minTickDistance || yScalingFactor*Math.pow(2,skipTick) < minTickDistance) //if it needs to be larger
            skipTick += 1
        xSkip = Math.pow(2,skipTick)
        ySkip = Math.pow(2,skipTick)

        displayplot.requestPaint()
    }


    function onXYChanged(){
        //update offset values
        xOffset = dummyoffset.x+touchinteract.lastX+(dragpoint.x-touchinteract.prevX)
        yOffset = dummyoffset.y+touchinteract.lastY+(dragpoint.y-touchinteract.prevY)

        //axes offsets - stick them to the edge of they would move out of canvas
        if(xOffset >= displayplot.width-axesWidth) xAxisOffset = displayplot.width-axesWidth
        else if(xOffset < axesWidth) xAxisOffset = axesWidth
        else xAxisOffset = xOffset

        if(yOffset <= -displayplot.height+axesWidth) yAxisOffset = -displayplot.height+axesWidth
        else if(yOffset > -axesWidth) yAxisOffset = -axesWidth
        else yAxisOffset = yOffset

        //update canvas
        displayplot.requestPaint()
    }

    function fit(){
        //clear offsets
        dummyoffset.x = 0; dummyoffset.y = 0
        touchinteract.prevX = dragpoint.x; touchinteract.prevY = dragpoint.y
        touchinteract.lastX = 0; touchinteract.lastY = 0
        onXYChanged()
        //clear custom scale
        customScalingFactor = 0
    }

    //Make sure to update canvas if paint related properties are changed
    onValuesXChanged: displayplot.requestPaint()
    onValuesYChanged: displayplot.requestPaint()
    onAxesWidthChanged: displayplot.requestPaint()
    onTickHeightChanged: displayplot.requestPaint()
    onTickWidthChanged: displayplot.requestPaint()
    onCustomScalingFactorChanged: {
        if(!customScalingFactor)
            fitIntoCanvas()
    }

    Canvas{
        id: displayplot
        anchors.fill: parent

        function plotXYValues(ctx,xSeries,ySeries,zerordh){
            //find the minimum of the length of the two array - idealy it should be equal
            var minLength = xSeries.length > ySeries.length ? ySeries.length : ySeries.length
            //move to the first point
            if(minLength > 0)
                ctx.moveTo( xSeries[0]*xScalingFactor + xOffset, height -ySeries[0]*yScalingFactor + yOffset)
            //plot all of the points
            for(var i = 1; i < minLength; i++){
                if(zerordh) ctx.lineTo(xSeries[i-1]*xScalingFactor + xOffset, height -ySeries[i]*yScalingFactor + yOffset)
                ctx.lineTo( xSeries[i]*xScalingFactor + xOffset, height -ySeries[i]*yScalingFactor + yOffset)
            }
        }

        onPaint: {

            //get drawing context
            var ctx = getContext("2d")
            ctx.reset() //clear the canvas

            //variable for iterating
            var i

            //DEFINING VARIABLES FOR SCALING, MOVING, ETC.

            //position of first tick to draw - contains correction due to offset and scaling, therefore it is possible to draw minimal number of ticks
            var xStart = xOffset-Math.ceil(xOffset/xScalingFactor/xSkip)*xScalingFactor*xSkip
            var xNumOffset = (Math.floor(xOffset/xScalingFactor/xSkip)+(xStart === 0 ? 0 : 1))*xSkip
            var xTextOffset = yAxisOffset < -height+10 ? -15 : +5

            var yStart = (yOffset+height)-Math.ceil((yOffset+height)/yScalingFactor/ySkip)*yScalingFactor*ySkip
            var yNumOffset = (Math.floor((yOffset+height)/yScalingFactor/ySkip)+(yStart === 0 ? 0 : 1))*ySkip
            var yTextOffset = xAxisOffset > width-10 ? 15 : -3

            //DISPLAYING THE AXES

            ctx.beginPath()
            //draw x axes line
            ctx.moveTo(0,height+yAxisOffset)
            ctx.lineTo(width,height+yAxisOffset)
            //draw y axes line
            ctx.moveTo(xAxisOffset,0)
            ctx.lineTo(xAxisOffset,height)
            //stroke the axes lines
            ctx.lineWidth = axesWidth
            ctx.strokeStyle = Qt.rgba(0,0,0,1)
            ctx.stroke()

            //PLOT XY VALUES
            ctx.lineWidth = lineWidth

            //determine if we got a single data series or multiple ones
            var currXSeries = valuesX[0]
            var currYSeries = valuesY[0]
            if(currXSeries.length && currYSeries.length){
                //this is for multiple data series
                //find min
                var minLength = valuesX.length > valuesY.length ? valuesY.length : valuesX.length
                for(var j = 0; j < minLength; j++){
                    //every line is a new path to be able to apply different colors
                    ctx.beginPath()

                    //detremine if zeroOrderHold is specified and if it a single value or an array
                    if(zeroOrderHold && zeroOrderHold.length)
                        plotXYValues(ctx,valuesX[j],valuesY[j],zeroOrderHold[j] ? 1 : 0)
                    else
                        plotXYValues(ctx,valuesX[j],valuesY[j],zeroOrderHold ? 1 : 0)

                    //determine of colors are specified for lines and if it a single value or an array, pick a color from palette otherwise
                    if(lineColors){
                        if(lineColors.length){
                            if(lineColors[j] && lineColors[j] !== 0)
                                ctx.strokeStyle = lineColors[j]
                            else
                                ctx.strokeStyle = Material.color((j*2)%20)
                        }
                        else ctx.strokeStyle = lineColors
                    }
                    else
                        ctx.strokeStyle = Material.color((j*2)%20)

                    //draw the line
                    ctx.stroke()
                }
            }
            else{
                ctx.beginPath()
                //this is for single data serie - logic is the same for multiple ones, only the indexing is different
                if(zeroOrderHold && zeroOrderHold.length)
                    plotXYValues(ctx,valuesX,valuesY,zeroOrderHold[0] ? 1 : 0)
                else
                    plotXYValues(ctx,valuesX,valuesY,zeroOrderHold ? 1 : 0)
                if(lineColors){
                    if(lineColors.length)
                        ctx.strokeStyle = lineColors[0]
                    else
                        ctx.strokeStyle = lineColors
                }
                else ctx.strokeStyle = Material.color(0)

                ctx.stroke()
            }

            ctx.beginPath()
            ctx.font = "10px sans-serif"
            //draw ticks on x axes
            var counter = 0
            for(i = xStart; i <= width; i += xScalingFactor*xSkip){
                //draw tick
                ctx.moveTo(i, height+tickHeight +yAxisOffset)
                ctx.lineTo(i, height-tickHeight +yAxisOffset)
                //draw its number
                if(showAxesNumbers === 1 || showAxesNumbers > 2) ctx.fillText( counter-xNumOffset,i, height+yAxisOffset-xTextOffset)
                counter += xSkip
            }
            //draw ticks on y axes
            counter = 0
            for(i = yStart; i <= height+10; i += yScalingFactor*ySkip){
                //draw tick
                ctx.moveTo( tickHeight +xAxisOffset,i)
                ctx.lineTo(-tickHeight +xAxisOffset,i)
                //draw its number
                if(showAxesNumbers >= 2 && counter-yNumOffset !== 0) ctx.fillText( -(counter-yNumOffset),-yTextOffset+xAxisOffset,i)
                counter += ySkip
            }
            ctx.lineWidth = tickWidth
            ctx.strokeStyle = Qt.rgba(0,0,0,1)
            ctx.stroke()
        }

        onWidthChanged: {
            if(!customScalingFactor)
                fitIntoCanvas()
            requestPaint()
        }
        onHeightChanged: {
            if(!customScalingFactor)
                fitIntoCanvas()
            requestPaint()
        }
    }
    MouseArea{
        id: interactarea
        anchors.fill: parent
        hoverEnabled: true
        scrollGestureEnabled: false
        preventStealing: true
        drag.target: dummyoffset
        drag.smoothed: false
        onWheel: {
            //scale the plot on wheel scroll
            customScalingFactor = true
            xScalingFactor *= (1+(wheel.angleDelta.y-wheel.angleDelta.x)/1000)
            yScalingFactor *= (1+(wheel.angleDelta.y)/1000)

            //update the canvas
            updateTickSkip()
        }
        onDoubleClicked: fit()
        PinchArea{
            id: pinchinteract
            anchors.fill: parent
            onPinchUpdated: {
                if(!customScalingFactor) customScalingFactor = true
                xScalingFactor *= Math.pow(pinch.scale,0.02)
                yScalingFactor *= Math.pow(pinch.scale,0.02)

                updateTickSkip()
            }
            MultiPointTouchArea{
                id: touchinteract
                property double prevX: 0
                property double prevY: 0
                property double lastX: 0
                property double lastY: 0
                property double startDistance: 0
                anchors.fill: parent
                mouseEnabled: false
                touchPoints: [TouchPoint{id: dragpoint},TouchPoint{id: pinchpoint}]
                onPressed: {
                    prevX = dragpoint.x; prevY = dragpoint.y;
                    if(!doubletouchCounter.running) doubletouchDelay.start()
                    else if(!pinchpoint.pressed) fit()
                    onXYChanged()
                }
                onReleased: {
                    lastX += dragpoint.x-prevX;
                    lastY += dragpoint.y-prevY;
                    prevX = dragpoint.x;
                    prevY = dragpoint.y;
                    onXYChanged()
                }
            }
            Timer{
                id: doubletouchDelay
                interval: 50
                onTriggered: doubletouchCounter.start()
            }
            Timer{
                id: doubletouchCounter
                interval: 150
            }
            Rectangle{
                id: dummydrag
                x: dragpoint.x
                y: dragpoint.y

                onXChanged: onXYChanged()
                onYChanged: onXYChanged()
            }
        }

        Rectangle{
            id: dummyoffset

            onXChanged: onXYChanged()
            onYChanged: onXYChanged()
        }
    }
}
