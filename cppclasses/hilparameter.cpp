#include "hilparameter.h"

#include <iostream>

int64_t HILParameter::applyMask(const int64_t &value){
    //determine mask size
    uint64_t maskCopy = Mask;
    int size = 0;
    while(maskCopy > 0){
        ++size;
        maskCopy = maskCopy >> 1;
    }

    int64_t realValue = 0;
    unsigned long shiftNeeded = 0;
    for(int currentShift = 0; currentShift < size; currentShift++){
        uint64_t selectMask = static_cast<uint64_t>(1) << currentShift;
        bool currentBit = static_cast<uint64_t>(value) & Mask & selectMask;
        realValue += static_cast<int64_t>(currentBit << shiftNeeded);
        if((Mask >> currentShift)%2)
            ++shiftNeeded;
    }

    return realValue;
}

int64_t HILParameter::inverseMask(){return inverseMask(getValue());}

int64_t HILParameter::inverseMask(const int64_t &value){
    //determine mask size
    uint64_t maskCopy = Mask;
    unsigned long size = 0;
    while(maskCopy > 0){
        ++size;
        maskCopy = maskCopy >> 1;
    }

    uint64_t uvalue = static_cast<uint64_t>(value);

    int64_t outValue = 0;
    long shiftNeeded = -1;
    for(unsigned long currentShift = 0; currentShift < size; currentShift++){
        if( (Mask >> currentShift)%2)
            ++shiftNeeded;
        if( (uvalue >> shiftNeeded)%2 )
            outValue += static_cast<int64_t>(1) << currentShift;

        //std::cout << "Current shift: " << currentShift << std::endl << "Current value: " << outValue << std::endl;
    }

    return outValue;
}
