import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

Item {
    property int margins: 5
    property int defHeight: 50
    property int settingsPadding: 10
    Layout.minimumHeight: defHeight
    clip: true

    Rectangle{
        id: borderRect
        anchors.fill: parent
        border.color: Qt.rgba(0,0,0,0.15)
        border.width: 2
        radius: 10
    }

    Button{
        id: moveButton
        anchors{left: parent.left; top: parent.top; bottom: title.bottom}
        width: 10

        background: Rectangle{
            anchors{left: parent.left; top: parent.top; bottom: parent.bottom;
                    leftMargin: 5; topMargin: borderRect.radius; bottomMargin: borderRect.radius}
            width: 2
            color: borderRect.border.color
        }
    }

    RowLayout{
        id: title
        anchors{left: moveButton.right; top: parent.top; right: deleteButton.left}
        anchors{leftMargin: 10; rightMargin: 5}
        height: defHeight-anchors.topMargin*2
        spacing: 0

        Text{
            Layout.maximumHeight: parent.height
            Layout.maximumWidth: 100
            Layout.alignment: Qt.AlignLeft
            text: variableName
            font.weight: Font.Light
        }
        TextField{
            Layout.maximumHeight: parent.height*0.6
            Layout.maximumWidth: 50
            Layout.alignment: Qt.AlignRight
            text: variableValue
        }
    }

    RoundButton{
        id: deleteButton
        text: qsTr("X")
        font.pixelSize: Qt.application.font.pixelSize*0.8
        anchors{right: parent.right; verticalCenter: title.verticalCenter; rightMargin: 5}
        width: 20
        height: defHeight*0.6
        flat: true
        hoverEnabled: true
        Rectangle{
            id: hoverBackground
            anchors{fill: parent;}
            color: Qt.rgba(0,0,0,0.1)
            radius: borderRect.radius
            visible: false
        }
        onHoveredChanged: hoverBackground.visible = !hoverBackground.visible

        onClicked: {
            availableVariablesModel.append(selectedVariablesModel.get(index))
            selectedVariablesModel.remove(index)
        }
    }

    Button{
        id: showSettingsButton
        anchors{bottom: parent.bottom; left: parent.left; right: parent.right}
        height: 20
        font.pixelSize: Qt.application.font.pixelSize*0.2
        background:
            Rectangle{
                anchors{horizontalCenter: parent.horizontalCenter; horizontalCenterOffset: -height/2*0.707+width/4;
                        bottom: parent.bottom; bottomMargin: borderRect.border.width*2}
                width: 1
                height: 6
                rotation: -45
                color: Qt.rgba(0,0,0,1)
                antialiasing: true
            }

            Rectangle{
                anchors{horizontalCenter: parent.horizontalCenter; horizontalCenterOffset: height/2*0.707-width/4
                        bottom: parent.bottom; bottomMargin: borderRect.border.width*2}
                width: 1
                height: 6
                rotation:  45
                color: Qt.rgba(0,0,0,1)
                antialiasing: true
            }
        onClicked: {
            if(parent.state === "") parent.state = "showsettings"
            else parent.state = ""
        }
    }

    GridLayout{
        id: settingsPanel
        anchors{left: title.left; top: title.bottom; right: title.right;
                topMargin: 10; leftMargin: settingsPadding; rightMargin: settingsPadding}
        columns: 2
        Rectangle{
            Layout.columnSpan: 2
            Layout.preferredWidth: parent.width/4
            Layout.maximumHeight: 1
            Layout.preferredHeight: Layout.maximumHeight
            Layout.bottomMargin: 15
            Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
            color: Qt.rgba(0,0,0,0.0)
        }
        Text{
            id: settingDatatypeName
            text: qsTr("Data type")
        }
        ComboBox{
            id: settingDatatypeValue
            Layout.maximumWidth: 80
            Layout.maximumHeight: settingDatatypeName.height*1.5
            Layout.alignment: Qt.AlignRight
            padding: 0
            model: ["int", "double"]
        }

        Text{
            id: settingUpdateintervalName
            text: qsTr("Update interval (ms)")
        }
        TextField{
            id: settingUpdateintervalValue
            Layout.maximumWidth: 80
            Layout.maximumHeight: settingUpdateintervalName.height*1.5
            Layout.alignment: Qt.AlignRight
            padding: 0
            text: qsTr("100")
        }
    }

    states:[
        State{
            name: "showsettings"
            PropertyChanges {
                target: borderRect.parent
                Layout.minimumHeight: defHeight + settingsPanel.height + 30
            }
            PropertyChanges {
                target: showSettingsButton
                rotation: 180
            }
        }
    ]

    transitions: [
        Transition {
            from: ""
            to: "showsettings"
            PropertyAnimation{target: borderRect.parent; property: "Layout.minimumHeight"; duration: 150}
            NumberAnimation{target: showSettingsButton; properties: "rotation,anchors.bottomMargin"; duration: 250}
        },
        Transition {
            from: "showsettings"
            to: ""
            PropertyAnimation{target: borderRect.parent; property: "Layout.minimumHeight"; duration: 150}
            NumberAnimation{target: showSettingsButton; properties: "rotation,anchors.bottomMargin"; duration: 250}
        }
    ]
}
