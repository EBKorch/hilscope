#include "hilparameterhandler.h"


HILParameterHandler::HILParameterHandler(const std::string& name, const std::string& parameterfile, HILAbstractInterface& interface):
    Name(name), ParameterFile(parameterfile), Interface(&interface), pendingIsBusy(false){
    // Open input configuration file
    std::ifstream configfile(parameterfile);
    // Will contain one line at a time which should hold all information regarding a parameter
    std::string line;
    if(configfile.is_open()){
        while(getline(configfile,line)){
            // Search for name
            while(line.at(0) == ' ') line = line.substr(1);
            std::string name = line.substr(0,line.find(" "));
            line = line.substr(line.find(" "));
            // Search for memory address
            while(line.at(0) == ' ') line = line.substr(1);
            std::string memoryaddress = line.substr(0,line.find(" "));
            line = line.substr(line.find(" "));
            // Search for type
            std::string type = line.substr(line.find("%"));
            type = type.substr(1,type.find(" ")-1);
            // Search for mask if exists
            std::string mask = "";
            if (line.find("#MASK=") != std::string::npos){
                mask = line.substr(line.find("#MASK="));
                mask = mask.substr(6,mask.find(" "));
            }
            else mask = "0x000000";

            //Create new parameter
            if(type == "U16"){
                int16_t val = 0;
                HILParameter temp(name,std::stoul(memoryaddress,nullptr,16),std::stoul(mask,nullptr,16),HILParameter::DataType::U16,val);
                Parameters.push_back(temp);
            }
            else if(type == "U32"){
                int32_t val = 0;
                HILParameter temp(name,std::stoul(memoryaddress,nullptr,16),std::stoul(mask,nullptr,16),HILParameter::DataType::U32,val);
                Parameters.push_back(temp);
            }
            else if(type == "U64"){
                int64_t val = 0;
                HILParameter temp(name,std::stoul(memoryaddress,nullptr,16),std::stoul(mask,nullptr,16),HILParameter::DataType::U64,val);
                Parameters.push_back(temp);
            }
            else if(type == "S16"){
                int16_t val = 0;
                HILParameter temp(name,std::stoul(memoryaddress,nullptr,16),std::stoul(mask,nullptr,16),HILParameter::DataType::S16,val);
                Parameters.push_back(temp);
            }
            else if(type == "B16"){
                bool val = 0;
                HILParameter temp(name,std::stoul(memoryaddress,nullptr,16),std::stoul(mask,nullptr,16),HILParameter::DataType::BOOL,val);
                Parameters.push_back(temp);
            }
            else if(type == "FLOAT"){
                int64_t val = 0;
                HILParameter temp(name,std::stoul(memoryaddress,nullptr,16),std::stoul(mask,nullptr,16),HILParameter::DataType::FLOAT,val);
                Parameters.push_back(temp);
            }
        }

        configfile.close();
    }
    else std::cout << "Could not open configuration file. No parameter will be present." << std::endl;
}

unsigned long HILParameterHandler::getByName(const std::string &name){
    auto iterator = std::find_if(Parameters.begin(),Parameters.end(),
                                 [&name](const HILParameter &param){
                                    return param.getName() == name;}
                                );
    if (iterator != Parameters.end())
        return static_cast<unsigned long>(std::distance(Parameters.begin(),iterator));

    return 0;
}


unsigned long HILParameterHandler::getByMemoryAddress(const uint64_t &memoryaddress, const unsigned long &n){
    unsigned long count = 0;
    auto iterator = std::find_if(Parameters.begin(),Parameters.end(),
                                 [&memoryaddress,&n,&count](const HILParameter &param){
                                    return (param.getMemoryAddress() == memoryaddress) && count++==n;}
                                );
    if (iterator != Parameters.end())
        return static_cast<unsigned long>(std::distance(Parameters.begin(),iterator));

    return 0;
}

void HILParameterHandler::updateValues(std::vector<unsigned long> internalids){

    std::unordered_set<uint64_t> memoryAddresses_set;
    for(unsigned long i = 0; i < internalids.size(); i++)
        memoryAddresses_set.insert(Parameters[internalids[i]].getMemoryAddress());

    std::vector<uint64_t> memoryAddresses(memoryAddresses_set.begin(),memoryAddresses_set.end());

    std::vector<HILStreamUnit> updated = Interface->recieveSpecificInput(memoryAddresses);

    for(unsigned long i = 0; i < updated.size(); i++){
        uint64_t currentAddress = updated[i].getMemoryAddress();
        int64_t currentValue = updated[i].getValue();

        unsigned long pos = getByMemoryAddress(currentAddress,0);
        if(Parameters[pos].getMemoryAddress() == currentAddress){
            unsigned long n = 1;
            do{
                Parameters[pos].setValue(currentValue);
                pos = getByMemoryAddress(currentAddress,n);
                ++n;
            }while(Parameters[pos].getMemoryAddress() == currentAddress && pos != 0);
        }
    }
}

int HILParameterHandler::pushChangeToValue(const unsigned long& internalid, const double& newvalue){

    //convert the value if it is real double
    int64_t convalue;
    if(std::ceil(newvalue) != std::floor(newvalue) || getType(internalid) == HILParameter::DataType::FLOAT){
        std::memcpy(&convalue,&newvalue,sizeof(double));
    }
    else convalue = static_cast<int64_t>(newvalue);

    //if pending changes are currently modified, use a temporary container
    std::vector<HILParameterHandler::Pending> *Pendings;
    if(pendingIsBusy) Pendings = &TemporaryPendingChanges;
    else Pendings = &PendingChanges;

    //search if a previous change requested previously
    auto iter = std::find_if(Pendings->begin(), Pendings->end(), [&internalid](Pending &pnd){return pnd.internalid == internalid;});

    //act accordingly
    if(iter != Pendings->end()){
        iter->newValue = convalue;
        //std::cout << "It is a change to an existing value" << std::endl;
    }
    else{
        Pending temp = {internalid, convalue};
        Pendings->push_back(temp);
        //std::cout << "It is a new value" << std::endl;
    }

    /*for(unsigned long i = 0; i < PendingChanges.size(); i++)
        std::cout << PendingChanges[i].internalid << " - val: " << PendingChanges[i].newValue << std::endl;*/

    return 0;
}

void HILParameterHandler::pushPendingChanges(){
    struct addressvalue{
        uint64_t address;
        int64_t value;
    };

    //pending should not be changed during data evaluation
    this->pendingIsBusy = true;

    //convert internalid-value pairs to memoryaddress-value pairs
    std::vector<addressvalue> allAddress;
    std::vector<uint64_t> requestedAddresses;
    for(unsigned long i = 0; i < PendingChanges.size(); i++){
        addressvalue temp = {getMemoryAddress(PendingChanges[i].internalid), PendingChanges[i].newValue};
        unsigned long j;
        for(j = 0; j < allAddress.size(); j++){
            if(requestedAddresses[j] == temp.address)
                break;
        }
        if(j >= allAddress.size() || requestedAddresses[j] != temp.address) requestedAddresses.push_back(temp.address);
        allAddress.push_back(temp);
    }

    //request current values from memory addresses
    std::vector<HILStreamUnit> currentValuesOnAddresses = Interface->recieveSpecificInput(requestedAddresses);
    /*for(unsigned long i = 0; i < currentValuesOnAddresses.size(); i++)
        std::cout << "Rec. addr.: " << currentValuesOnAddresses[i].getMemoryAddress() << " with value: " <<
                     currentValuesOnAddresses[i].getValue() << std::endl;*/

    //iterate through all addres-value pair, unite multiple addresses
    std::vector<HILStreamUnit> allToWrite;
    for(unsigned long i = 0; i < allAddress.size(); i++){
        int64_t currentValue = allAddress[i].value;
        uint64_t currentAddress = allAddress[i].address;
        uint64_t currentID = PendingChanges[i].internalid;
        uint64_t currentMask = Parameters[currentID].getMask();

        //std::cout << "ID " << currentID << " value: " << currentValue << " mask: " << currentMask << std::endl;

        if(currentMask != 0){
            currentValue = Parameters[currentID].inverseMask(currentValue);
            //std::cout << "Value is masked according! " << currentValue << std::endl;
        }
        //else std::cout << "Value do not need masking" << std::endl;

        //check if allToWrite allready contains the address
        auto iterator = std::find_if(allToWrite.begin(), allToWrite.end(),
                                    [&currentAddress](const HILStreamUnit &tmp){return tmp.getMemoryAddress() == currentAddress;});

        if(iterator == allToWrite.end()){
            //std::cout << "It is a new write!" << std::endl;
            auto iteratorH = std::find_if(currentValuesOnAddresses.begin(), currentValuesOnAddresses.end(),
                                         [&currentAddress](const HILStreamUnit &tmp){return tmp.getMemoryAddress() == currentAddress;});

            int64_t value = 0;
            if(iteratorH != currentValuesOnAddresses.end()){
                //theres knewn value for that addres, make it equal with it
                value = iteratorH->getValue();
            }
            if(currentMask)
                value -= static_cast<int64_t>((static_cast<uint64_t>(value) & currentMask));
            else
                value = 0;
            HILStreamUnit tmp(currentAddress, value + currentValue );
            //std::cout << "Pushed back value: " << tmp.getValue() << std::endl;
            allToWrite.push_back(tmp);
        }
        else{
            int64_t value = iterator->getValue();
            if(currentMask)
                value -= static_cast<int64_t>((static_cast<uint64_t>(value) & currentMask));
            else
                value = 0;
            iterator->setValue(value + currentValue);
            //std::cout << "Changed value to: " << iterator->getValue() << std::endl;
        }
    }

    /*for(unsigned long i = 0; i < allToWrite.size(); i++){
        std::cout << allToWrite[i].getMemoryAddress() << " - " << allToWrite[i].getValue() << std::endl;
    }*/

    PendingChanges.clear();

    this->pendingIsBusy = false;

    Interface->sendOutput(allToWrite);

    if(TemporaryPendingChanges.size() > 0)
        std::cout << "Not yet implemented" << std::endl;
}
