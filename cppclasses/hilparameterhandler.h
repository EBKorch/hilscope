#ifndef HILPARAMETERHANDLER_H
#define HILPARAMETERHANDLER_H

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <thread>
#include <cmath>
#include <cstring>

#include "hilparameter.h"
#include "hilabstractinterface.h"

class HILParameterHandler
{
    //Identifier for the handler. Different handlers can be used with different interfaces
    //(for different communications and parameter names
    std::string Name;

    //Conatins the name, memory address, data type etc. properties of parameters
    std::string ParameterFile;

    //Contains the parameters (initialized according to the parameter file) and values from last update
    std::vector<HILParameter> Parameters;

    //The interface for communication with the hardware
    HILAbstractInterface *Interface;

    //Vector containing the previous values from update

    //Changes requested by the user, waiting to be written to the hardware
    struct Pending{
        unsigned long internalid;
        int64_t newValue;
    };
    std::vector<Pending> PendingChanges;
    bool pendingIsBusy;
    std::vector<Pending> TemporaryPendingChanges;
public:
    HILParameterHandler(const std::string& name, const std::string& parameterfile, HILAbstractInterface& interface);

    //Value manipulation
    void updateValues(std::vector<unsigned long> internalids);
    int pushChangeToValue(const unsigned long& internalid, const double &newvalue);
    void pushPendingChanges();

    //Access parameter properties
    std::string getName(const unsigned long& internalid) const {return Parameters[internalid].getName();}
    HILParameter::DataType getType(const unsigned long& internalid) const {return Parameters[internalid].getType();}
    int64_t getValue(const unsigned long& internalid) const {return Parameters[internalid].getValue();}
    uint64_t getMemoryAddress(const unsigned long& internalid) const {return Parameters[internalid].getMemoryAddress();}
    unsigned long getParameterCount(){return Parameters.size();}

    //Access parameter internal id (position in vector)
    unsigned long getByName(const std::string& name);
    unsigned long getByMemoryAddress(const uint64_t& memoryaddress, const unsigned long& n);
};

#endif // HILPARAMETERHANDLER_H
