#ifndef HILSTREAMUNIT_H
#define HILSTREAMUNIT_H

#include <cstdint>

class HILStreamUnit
{
    //Enumeration for every possible type from input stream
    enum UniType{
        N64,
        N32,
        N16
    };

public:
    //A union which can hold 64/32/16 bit or bool value
    union UniValue{
        UniValue(const int64_t& value): Value64(value){}
        UniValue(const int32_t& value): Value32(value){}
        UniValue(const int16_t& value): Value16(value){}
        int64_t get64Value() const {return Value64;}
        int32_t get32Value() const {return Value32;}
        int16_t get16Value() const {return Value16;}
        void setValue(const int64_t& value){Value64 = value;}
        void setValue(const int32_t& value){Value32 = value;}
        void setValue(const int16_t& value){Value16 = value;}
    private:
        int64_t Value64;
        int32_t Value32;
        int16_t Value16;
    };

    //Constructors for different size of data. Actual handling is done based on Type identifier.
    HILStreamUnit(const uint64_t& memoryAddress, const int64_t& value):
        MemoryAddress(memoryAddress),  Value(value){Type = UniType::N64;}
    HILStreamUnit(const uint64_t& memoryAddress, const int32_t& value):
        MemoryAddress(memoryAddress),  Value(value){Type = UniType::N32;}
    HILStreamUnit(const uint64_t& memoryAddress, const int16_t& value):
        MemoryAddress(memoryAddress),  Value(value){Type = UniType::N16;}

    //Obtaining data from the object
    uint64_t getMemoryAddress() const{return MemoryAddress;}
    int64_t getValue() const {
             if(Type == UniType::N64)  return Value.get64Value();
        else if(Type == UniType::N32)  return Value.get32Value();
        else if(Type == UniType::N16)  return Value.get16Value();

        return 0;
    }

    void setValue(const int64_t &newValue){
            if(Type == UniType::N64)  setValue(newValue);
    }

private:
    uint64_t MemoryAddress;
    UniValue Value;
    UniType Type;
};

#endif // HILSTREAMUNIT_H
